package br.calculaloria;

import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

import br.calculaloria.database.SqliteDatabase;
import br.calculaloria.food.Food;
import br.calculaloria.food.repositories.FoodRepository;

@RunWith(AndroidJUnit4.class)
public class TestFoodRepository {

    private Context context = InstrumentationRegistry.getTargetContext();
    private SqliteDatabase database;

    @Before
    public void openDatabase() {
        this.database = new SqliteDatabase(this.context);
    }

    @After
    public void cleanDatabase() {
        this.context.deleteDatabase(this.database.getDatabaseName());
    }

    @Test
    public void testInitRepository() {
        FoodRepository repository = new FoodRepository( this.database );
        List<Food> received = repository.getAllFood();

        List<Food> expected = Arrays.asList(
                new Food(
                        1,
                        "Água de coco verde",
                        null,
                        "ml",
                        240,
                        62
                ),
                new Food(
                        2,
                        "Café com açúcar",
                        null,
                        "ml",
                        50,
                        33
                ),
                new Food(
                        3,
                        "Café sem açúcar",
                        null,
                        "ml",
                        40,
                        3
                ),
                new Food(
                        4,
                        "Caldo de cana",
                        null,
                        "ml",
                        240,
                        202
                ),
                new Food(
                        5,
                        "Suco de abacaxi natural",
                        null,
                        "ml",
                        240,
                        100
                ),
                new Food(
                        6,
                        "Suco de acerola natural",
                        null,
                        "ml",
                        240,
                        36
                ),
                new Food(
                        7,
                        "Suco de maçã natural",
                        null,
                        "ml",
                        240,
                        154
                )
        );

        Assert.assertArrayEquals(expected.toArray(), received.toArray());
    }


    @Test
    public void testChangeFood() {
        FoodRepository repository = new FoodRepository( this.database );
        List<Food> foods = repository.getAllFood();

        Food food = foods.get(0);
        food.setName( food.getName() + "EU MUDEI SSA PORRA ");

        Food received = this.database.loadFood(1);
        Assert.assertEquals(food, received);
    }
}
