package br.calculaloria;

import android.content.Context;
import android.util.Log;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.calculaloria.food.Food;
import br.calculaloria.database.SqliteDatabase;

@RunWith(AndroidJUnit4.class)
public class SqliteDatabaseTest {

    @After
    public void deleteDatabase() {
        InstrumentationRegistry.getTargetContext().deleteDatabase("banco.db");
    }

    @Test
    public void testSqliteDatabaseInit() {
//        Context context = InstrumentationRegistry.getTargetContext();
//
//        SqliteDatabase database = new SqliteDatabase(context);
//
//        List<Food> expects = Arrays.asList(
//                new Food(
//                        "Água de coco verde",
//                        null,
//                        "ml",
//                        240,
//                        62
//                ),
//                new Food(
//                        "Café com açúcar",
//                        null,
//                        "ml",
//                        50,
//                        33
//                ),
//                new Food(
//                        "Café sem açúcar",
//                        null,
//                        "ml",
//                        40,
//                        3
//                )
//        );
//
//        List<Food> foods = database.getAllFood();
//
//        Assert.assertArrayEquals(
//                "Food is different than expected!",
//                expects.toArray(),
//                foods.toArray()
//        );
    }

    @Test
    public void testSqliteAdd() {
        Context context = InstrumentationRegistry.getTargetContext();
        SqliteDatabase database = new SqliteDatabase(context);
        Food food = new Food(
                "Aff2",
                new byte[1],
                "ml",
                256,
                45
        );
        long result = database.insertFood(food);
        Log.d("TestAdd", String.format("Result: %d", result));
        Assert.assertTrue(result > 0);
    }

    @Test
    public void testGetFood() {
        Context context = InstrumentationRegistry.getTargetContext();
        SqliteDatabase database = new SqliteDatabase(context);
        Food food = database.loadFood("Água de coco verde");
        Assert.assertTrue( food.getName().equals("Água de coco verde") );
    }
}
