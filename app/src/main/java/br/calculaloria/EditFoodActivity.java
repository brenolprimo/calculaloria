package br.calculaloria;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.List;

import br.calculaloria.calculator.CalorieCalculator;
import br.calculaloria.calculator.HistoryRow;
import br.calculaloria.database.SqliteDatabase;
import br.calculaloria.food.Food;
import br.calculaloria.food.repositories.FoodRepository;

public class EditFoodActivity extends AppCompatActivity {

    private boolean _changedImage = false;
    private ImageView _image;
    private Button _buttonImage, _buttonEdit;
    private EditText _editTextName, _editTextMeasurement, _editTextCalorie, _editTextQuantity;

    private static final int IMAGE_PICK_CODE = 1000;
    private static final int PERMISSION_CODE = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_food);

        this.init();
    }

    private void init() {
        _image                  = findViewById(R.id.imageview_edit_image);
        _buttonImage            = findViewById(R.id.button_edit_image_choose);
        _buttonEdit              = findViewById(R.id.button_edit_edit);
        _editTextName           = findViewById(R.id.edittext_edit_name);
        _editTextMeasurement    = findViewById(R.id.edittext_edit_measurement);
        _editTextCalorie        = findViewById(R.id.edittext_edit_calories);
        _editTextQuantity       = findViewById(R.id.edittext_edit_quantity);

        //final Food food = (Food) this.getIntent().getExtras().getSerializable("food");
        String foodName = this.getIntent().getExtras().getString("foodname");

        FoodRepository repository = new FoodRepository(new SqliteDatabase(this));
        Food food = null;
        for(int i = 0; i < repository.getAllFood().size(); i++) {
            if( repository.getAllFood().get(i).getName().equals(foodName) ) {
                food = repository.getAllFood().get(i);
                break;
            }
        }

        final Food tmp = food;

        if( food == null )
            return;

        Bitmap bitmap = BitmapFactory.decodeByteArray(food.getImage(), 0, food.getImage().length);
        _image.setImageBitmap(bitmap);
        _editTextName.setText(food.getName());
        _editTextMeasurement.setText(food.getMeasurement());
        _editTextCalorie.setText(String.valueOf(food.getCalories()));
        _editTextQuantity.setText(String.valueOf(food.getQuantity()));

        View.OnClickListener editListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = _editTextName.getText().toString();
                String measurement = _editTextMeasurement.getText().toString();
                int calorie = Integer.parseInt(_editTextCalorie.getText().toString() );
                int quantity = Integer.parseInt(_editTextQuantity.getText().toString());
                tmp.setName(name);
                tmp.setMeasurement(measurement);
                tmp.setCalories(calorie);
                tmp.setQuantity(quantity);

                if(_changedImage) {
                    byte[] array = null;

                    try {
                        Bitmap bitmap = ((BitmapDrawable) _image.getDrawable()).getBitmap();
                        ByteArrayOutputStream oStream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, oStream);
                        array = oStream.toByteArray();
                        oStream.close();
                    } catch(Exception e){
                        Log.d("CRASHOU", e.getMessage());
                        return;
                    }

                    if( array != null )
                        tmp.setImage(array);
                }

                Intent i = new Intent();
                i.putExtra("result", "ok");
                setResult(10001, i);

                FoodRepository repository = new FoodRepository(new SqliteDatabase(EditFoodActivity.this));
                CalorieCalculator calculator = CalorieCalculator.getInstance();
                calculator.reCalculate( repository.getAllFood() );

                Toast.makeText(EditFoodActivity.this, "Dados modificados com sucesso!", Toast.LENGTH_SHORT).show();
                finish();
            }
        };
        _buttonEdit.setOnClickListener(editListener);

        View.OnClickListener editImageListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ){
                    if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_DENIED )
                    {
                        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                        requestPermissions(permissions, PERMISSION_CODE);
                    }
                    else
                    {
                        getImageFromGallery();
                    }
                }
                else
                {
                    getImageFromGallery();
                }
            }
        };
        _buttonImage.setOnClickListener(editImageListener);
    }

    private void getImageFromGallery()
    {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode)
        {
            case PERMISSION_CODE: {
                if( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED )
                {
                    getImageFromGallery();
                }
                else
                {
                    Toast.makeText(this, "Permissão negada...", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if( resultCode == RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            _image.setImageURI(data.getData());
            _changedImage = true;
        }
    }
}
