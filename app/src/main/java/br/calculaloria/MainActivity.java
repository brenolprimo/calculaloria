package br.calculaloria;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import br.calculaloria.calculator.CalorieCalculator;
import br.calculaloria.calculator.FoodHistory;
import br.calculaloria.calculator.HistoryRow;
import br.calculaloria.database.Database;
import br.calculaloria.database.SqliteDatabase;
import br.calculaloria.food.Food;
import br.calculaloria.food.repositories.FoodRepository;
import br.calculaloria.ui.CalorieQuantity;
import br.calculaloria.ui.adapter.CatalogAdapter;
import br.calculaloria.ui.gambiarra.Invalidable;

public class MainActivity extends AppCompatActivity implements Invalidable {

    private CalorieCalculator _calculator = CalorieCalculator.getInstance();

    private TextView _textViewTotalCalories;
    private GridView _catalogGridView;
    private CatalogAdapter _adapter;
    private FoodRepository _repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _catalogGridView = this.findViewById(R.id.gridview_catalog);
        _repository = new FoodRepository(new SqliteDatabase(this.getApplicationContext()));
        _repository.addInvalid(this);

        _textViewTotalCalories = this.findViewById(R.id.textview_totalcalories);

        String message = String.format(
                Locale.US,
                "%d calorias",
                _calculator.getTotalCalorie()
        );
        _textViewTotalCalories.setText(message);

        PropertyChangeListener listener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                String message = String.format(
                        Locale.US,
                        "%d calorias",
                        (long) propertyChangeEvent.getNewValue()
                );
                _textViewTotalCalories.setText(message);
            }
        };
        _calculator.addListener(listener);

        _adapter = new CatalogAdapter(MainActivity.this, _repository);
        _catalogGridView.setAdapter(_adapter);

//        AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//                Toast.makeText(getApplicationContext(), "Clicked on " + position, Toast.LENGTH_SHORT ).show();
//            }
//        };

//        _catalogGridView.setOnItemClickListener(listener);
    }

    public void callCalorieInformation(Food food) {
//        DONT WORK
        Intent intent = new Intent(MainActivity.this, CalorieQuantity.class);
        intent.putExtra("CurrentFood", food);
//        FoodRepository.saveFood(food);
//
//        this.startActivityForResult(intent, Activity.RESULT_OK);

        this.startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.appmenu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.menu_list:
                this.showListDialog();
//                Intent listIntent = new Intent(MainActivity.this, ListActivity.class);
//                startActivity(listIntent);
                return true;
            case R.id.menu_reset:
                CalorieCalculator calculator = CalorieCalculator.getInstance();
                if( calculator.getHistory().size() < 1 ){
                    Toast.makeText(this, "Sem nenhum item para resetar!", Toast.LENGTH_SHORT).show();
                    return true;
                }

                calculator.clearHistory();
                Toast.makeText(this, "Resetado com sucesso!", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.menu_modify:
                Intent intent = new Intent(MainActivity.this, ModifyActivity.class);
                startActivity(intent);
                return true;
            case R.id.menu_save_history:
                final List<HistoryRow> calculationHistory = CalorieCalculator.getInstance().getHistory();
                if( calculationHistory.size() < 1 ){
                    Toast.makeText(this, "Nenhum item no histórico para ser salvo!", Toast.LENGTH_SHORT).show();
                    return true;
                }

                long result = CalorieCalculator.getInstance().saveHistory(new SqliteDatabase(this));
                if( result > 0 )
                    Toast.makeText(this, "O histórico foi salvo com sucesso!", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(this, "O histórico não foi salvo! Olhe o log :D", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.menu_save_recover:
                List<String> names = this.recoverHistoryNames(new SqliteDatabase(this));
                this.showRecoverNamesDialog(names);
                return true;
            case R.id.menu_save_remove:
                names = this.recoverHistoryNames(new SqliteDatabase(this));
                this.showRecoverDeleteDialog(names);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void invalidade() {
        _repository = new FoodRepository(new SqliteDatabase(this));
        _adapter = new CatalogAdapter(this, _repository);
        _catalogGridView.invalidateViews();
        _catalogGridView.setAdapter(_adapter);

        //_adapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showListDialog() {
        final List<HistoryRow> calculationHistory = CalorieCalculator.getInstance().getHistory();

        if( calculationHistory.size() < 1 ){
            Toast.makeText(this, "Nenhum item adicionado no cálculo!", Toast.LENGTH_SHORT).show();
            return;
        }

        String[] alertItems = new String[calculationHistory.size()];
        final boolean[] alertItemsChecked = new boolean[calculationHistory.size()];

        final List<Integer> itemsToBeRemoved = new ArrayList<>();

        for(int i = 0; i < calculationHistory.size(); i++) {
            alertItems[i] = calculationHistory.get(i).toString();
        }

        DialogInterface.OnMultiChoiceClickListener listener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int position, boolean isChecked) {
                if( isChecked ) {
                    if( !itemsToBeRemoved.contains(position) )
                        itemsToBeRemoved.add(position);
                } else if( itemsToBeRemoved.contains(position) ) {
                    itemsToBeRemoved.remove(position);
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Remover items do cálculo");
        builder.setMultiChoiceItems(alertItems, alertItemsChecked, listener);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Collections.reverse(itemsToBeRemoved);

                for(int value : itemsToBeRemoved) {
                    HistoryRow row = calculationHistory.get(value);
                    CalorieCalculator.getInstance().subtractCalorie(row);
                }
            }
        });
        builder.setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private List<String> recoverHistoryNames(SqliteDatabase db) {
        if(db == null)
            return new ArrayList<>(1);

        List<String> names = db.getAllFoodHistories();
        if( names == null )
            return new ArrayList<>(1);
        else
            return names;
    }

    private void showRecoverNamesDialog(final List<String> names)
    {
        String[] alertItems = new String[names.size()];
        int checkedItem = -1;

        for(int i = 0; i < names.size(); i++) {
            alertItems[i] = names.get(i);
        }
        final List<String> choosenItem = new ArrayList<>();

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int position) {
                if(choosenItem.size() > 0)
                    choosenItem.clear();
                choosenItem.add(names.get(position));
            }
        };

        DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                recoverHistory(choosenItem.get(0));
            }
        };

        DialogInterface.OnClickListener closeListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Selecione item para recuperar");
        builder.setSingleChoiceItems(alertItems, checkedItem, listener);
        builder.setPositiveButton("OK", okListener);
        builder.setNegativeButton("Fechar", closeListener);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void recoverHistory(String historyName) {
        Database db = new SqliteDatabase(this);
        FoodHistory history = db.getFoodHistory(historyName);

        if( history == null ) {
            Toast.makeText(this, "Ocorreu um erro ao recuperar o histórico!", Toast.LENGTH_SHORT).show();
            return;
        }

        FoodRepository repository = new FoodRepository(db);
        List<Food> allFood = repository.getAllFood();

        Iterator<HistoryRow> it = history.getFoodHistory().iterator();
        while(it.hasNext()){
            if( !(allFood.contains(it.next().getFood())) )
                it.remove();
        }

//        for(int i = 0; i < history.getFoodHistory().size(); i++){
//            if( !allFood.contains(history.getFoodHistory().get(i).getFood()) )
//                history.getFoodHistory().remove(i);
//        }

        CalorieCalculator.getInstance().loadFromHistory(history);
    }

    private void showRecoverDeleteDialog(List<String> names) {
        if( names.size() < 1 ){
            Toast.makeText(this, "Nenhum item salvo para remover!", Toast.LENGTH_SHORT).show();
            return;
        }

        String[] alertItems = new String[names.size()];
        final boolean[] alertItemsChecked = new boolean[names.size()];

        final List<String> tmp = names;
        final List<Integer> itemsToBeRemoved = new ArrayList<>();

        for(int i = 0; i < names.size(); i++) {
            alertItems[i] = names.get(i);
        }

        DialogInterface.OnMultiChoiceClickListener listener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int position, boolean isChecked) {
                if( isChecked ) {
                    if( !itemsToBeRemoved.contains(position) )
                        itemsToBeRemoved.add(position);
                } else if( itemsToBeRemoved.contains(position) ) {
                    itemsToBeRemoved.remove(position);
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Remover items salvos");
        builder.setMultiChoiceItems(alertItems, alertItemsChecked, listener);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Collections.reverse(itemsToBeRemoved);

                for(int value : itemsToBeRemoved) {
                    String name = tmp.get(value);
                    SqliteDatabase database = new SqliteDatabase(MainActivity.this);
                    database.removeHistory(name);
                }

                Toast.makeText(MainActivity.this, "Histórico deletado com sucesso!", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
