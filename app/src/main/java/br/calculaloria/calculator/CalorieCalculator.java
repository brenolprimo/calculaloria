package br.calculaloria.calculator;

import android.widget.Toast;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.calculaloria.database.Database;
import br.calculaloria.database.SqliteDatabase;
import br.calculaloria.food.Food;

public class CalorieCalculator implements Serializable
{
    private static CalorieCalculator _instance;

    private long _totalCalorie = 0;
    private FoodHistory _foodHistory = new FoodHistory();

    private PropertyChangeSupport _curiousPeople = new PropertyChangeSupport(this);

    public void addCalorie(int value, Food food) {
        HistoryRow row = new HistoryRow(food, value);
        _foodHistory.getFoodHistory().add(row);

        long oldTotalCalorie = _totalCalorie;
        _totalCalorie += Math.round(food.getBaseCalorie() * value);

        _curiousPeople.firePropertyChange("calorie", oldTotalCalorie, _totalCalorie);
    }

    public void subtractCalorie(HistoryRow row) {
        boolean result = _foodHistory.getFoodHistory().remove(row);

        if( !result ) {
            for(int i = 0; i < _foodHistory.getFoodHistory().size(); i++) {
                HistoryRow history = _foodHistory.getFoodHistory().get(i);
                if( history.getValue() == row.getValue() && history.getFood().equals(row.getFood()) ) {
                    _foodHistory.getFoodHistory().remove(i);
                    break;
                }
            }
        }

        long oldTotalCalorie = _totalCalorie;
        _totalCalorie -= Math.round(row.getFood().getBaseCalorie() * row.getValue());

        _curiousPeople.firePropertyChange("calorie", oldTotalCalorie, _totalCalorie);
    }

    public long getTotalCalorie() {
        return _totalCalorie;
    }

    public List<HistoryRow> getHistory() {
        return _foodHistory.getFoodHistory();
    }

    public void clearHistory() {
        _foodHistory.getFoodHistory().clear();
        long oldTotalCalorie = _totalCalorie;
        _totalCalorie = 0;
        _curiousPeople.firePropertyChange("calorie", oldTotalCalorie, _totalCalorie);
    }

    public void addListener(PropertyChangeListener listener) {
        _curiousPeople.addPropertyChangeListener(listener);
    }

    public void removeListener(PropertyChangeListener listener) {
        _curiousPeople.removePropertyChangeListener(listener);
    }

    public long saveHistory(Database db) {
        if( db == null )
            return -1;

        return db.saveHistory(_foodHistory);
    }

    public static CalorieCalculator getInstance() {
        if( _instance == null )
            _instance = new CalorieCalculator();
        return _instance;
    }

    private CalorieCalculator() {}

    public void loadFromHistory(FoodHistory history)
    {
        this.clearHistory();

        long oldTotalCalorie = 0;
        for(HistoryRow row : history.getFoodHistory()){
            _foodHistory.getFoodHistory().add(row);

            _totalCalorie += Math.round(row.getFood().getBaseCalorie() * row.getValue());
        }

        _curiousPeople.firePropertyChange("calorie", oldTotalCalorie, _totalCalorie);
    }

    public void reCalculate() {
        this.reCalculate(null);
    }

    public void reCalculate(List<Food> upToDateFoods){
        long oldCalorie = _totalCalorie;
        _totalCalorie = 0;

        this.syncronizeData(upToDateFoods);

        for(HistoryRow row : _foodHistory.getFoodHistory()){
            _totalCalorie += Math.round(row.getFood().getBaseCalorie() * row.getValue());
        }

        _curiousPeople.firePropertyChange("calorie", oldCalorie, _totalCalorie);
    }

    private void syncronizeData(List<Food> upToDateFoods){
        if( upToDateFoods == null )
            return;

        List<Integer> toRemoveList = new ArrayList<>();

        List<HistoryRow> currFoods = _foodHistory.getFoodHistory();
        for(int i = 0; i < currFoods.size(); i++){
            boolean toRemove = true;

            for(int j = 0; j < upToDateFoods.size(); j++){
                Food tmp = upToDateFoods.get(j);
                if( currFoods.get(i).getFood().getId() == tmp.getId() ) {
                    int value = currFoods.get(i).getValue();
                    HistoryRow row = new HistoryRow(tmp, value);
                    _foodHistory.getFoodHistory().set(i, row);
                    toRemove = false;
                }
            }

            if(toRemove)
                toRemoveList.add(i);
        }

        Collections.sort(toRemoveList);
        Collections.reverse(toRemoveList);
        for(int i : toRemoveList) {
            _foodHistory.getFoodHistory().remove(i);
        }
    }
}
