package br.calculaloria.calculator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.Locale;

import br.calculaloria.food.Food;

public class HistoryRow implements Serializable
{
    private Food _food;
    private int _value;

    public HistoryRow(Food food, int value) {
        _food = food;
        _value = value;
    }

    public Food getFood() {
        return _food;
    }

//    public void setFood(Food _food) {
//        this._food = _food;
//    }

    public int getValue() {
        return _value;
    }

//    public void setValue(int _value) {
//        this._value = _value;
//    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if( !(obj instanceof HistoryRow) )
            return false;

        HistoryRow o = (HistoryRow) obj;
        if( o._value != this._value )
            return false;

        if( !o._food.equals(this._food) )
            return false;

        return true;
    }

    @NonNull
    @Override
    public String toString() {
        String message = String.format(
                Locale.US,
                "%s %d%s - %d calorias",
                _food.toString(),
                _value,
                _food.getMeasurement(),
                Math.round( _food.getBaseCalorie() * _value )
        );
        return message;
    }
}
