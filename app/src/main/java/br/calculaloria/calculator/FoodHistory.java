package br.calculaloria.calculator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class FoodHistory implements Serializable
{
    private static long serialVersionUID = 1233445234;

    private int _id;
    private String _name;
    private List<HistoryRow> _foodHistory = new ArrayList<>(10);

    public FoodHistory() {
        _id = -1;
    }

    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }

    public String getName() {
        return getName(false);
    }

    public String getName(boolean forceRenaming) {
        if(  forceRenaming == true || _name == null || _name.equals("") )
        {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS", new Locale("pt", "BR"));
            _name = format.format(calendar.getTime());
        }
        return _name;
    }

    public List<HistoryRow> getFoodHistory() {
        return _foodHistory;
    }

    public void setFoodHistory(List<HistoryRow> _foodHistory) {
        this._foodHistory = _foodHistory;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(!(obj instanceof FoodHistory))
            return false;

        FoodHistory tmp = (FoodHistory) obj;
        return this._name.equals(tmp._name) && this._id == tmp._id;
    }

    @NonNull
    @Override
    public String toString() {
        if(_name == null)
            _name = "";
        return _name;
    }
}
