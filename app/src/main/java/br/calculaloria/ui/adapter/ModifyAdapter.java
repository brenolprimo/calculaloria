package br.calculaloria.ui.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import java.util.Iterator;
import java.util.List;

import br.calculaloria.ModifyActivity;
import br.calculaloria.R;
import br.calculaloria.calculator.CalorieCalculator;
import br.calculaloria.calculator.HistoryRow;
import br.calculaloria.database.SqliteDatabase;
import br.calculaloria.food.Food;
import br.calculaloria.food.repositories.FoodRepository;
import br.calculaloria.ui.gambiarra.Invalidable;

public class ModifyAdapter extends BaseAdapter {

    private Activity _context;
    private LayoutInflater _inflater;
    private FoodRepository _repository;

    public ModifyAdapter(@NonNull Activity activity, FoodRepository repository) {
        _context = activity;
        _repository = repository;
    }

    @Override
    public int getCount() {
        return _repository.getAllFood().size();
    }

    @Override
    public Object getItem(int i) {
        return _repository.getAllFood().get(i);
    }

    @Override
    public long getItemId(int i) {
        return _repository.getAllFood().get(i).getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if( _inflater == null )
            _inflater = _context.getLayoutInflater();

        if( view == null )
            view = _inflater.inflate(R.layout.layout_modify_row_item, null);

        this.init(position, view);

        return view;
    }

    private void init(int position, View view) {
        ImageView imageView = view.findViewById(R.id.imageview_edit_catalog);
        TextView name = view.findViewById(R.id.textview_edit_name);
        Button btnEdit = view.findViewById(R.id.button_edit_edit);
        Button btnRemove = view.findViewById(R.id.button_edit_remove);

        final Food food = _repository.getAllFood().get(position);
        name.setText(food.getName());

        Bitmap bitmap = BitmapFactory.decodeByteArray(food.getImage(), 0, food.getImage().length);
        imageView.setImageBitmap(bitmap);

        View.OnClickListener removeListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                food.deleteFood();
                Toast.makeText(ModifyAdapter.this._context, "Comida deletada com sucesso!", Toast.LENGTH_SHORT).show();

                FoodRepository repository = new FoodRepository(new SqliteDatabase(_context));
                List<Food> allValidFood = repository.getAllFood();

                CalorieCalculator calculator = CalorieCalculator.getInstance();
                Iterator<HistoryRow> it = calculator.getHistory().iterator();

                while(it.hasNext()){
                    if(!(allValidFood.contains(it.next().getFood())))
                        //calculator.subtractCalorie(it.next());
                        it.remove();
                }

                calculator.reCalculate();
                invalidadeCatalogs();
            }
        };
        btnRemove.setOnClickListener(removeListener);

        View.OnClickListener editListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ModifyActivity)_context).startEditActivity(food);
            }
        };
        btnEdit.setOnClickListener(editListener);
    }

    private void invalidadeCatalogs() {
        List<Invalidable> invalids = FoodRepository.getAllInvalidables();
        for(Invalidable invalid : invalids)
            invalid.invalidade();
    }
}
