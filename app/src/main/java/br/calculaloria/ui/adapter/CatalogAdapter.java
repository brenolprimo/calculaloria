package br.calculaloria.ui.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import br.calculaloria.MainActivity;
import br.calculaloria.R;
import br.calculaloria.food.Food;
import br.calculaloria.food.repositories.FoodRepository;

public class CatalogAdapter extends BaseAdapter {
    private Context _context;
    private FoodRepository _repository;
    private LayoutInflater _inflater;

    public CatalogAdapter(@NonNull Context activity, @NonNull FoodRepository repository) throws NullPointerException {
        _context = activity;
        _repository = repository;
    }

    @Override
    public int getCount() {
        return _repository.getAllFood().size();
    }

    @Override
    public Object getItem(int i) {
        return _repository.getAllFood().get(i);
    }

    @Override
    public long getItemId(int i) {
        return _repository.getAllFood().get(i).getId();
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if( _inflater == null )
            _inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if( view == null )
            view = _inflater.inflate(R.layout.layout_catalog_row_item, null);

        ImageView image = view.findViewById(R.id.imageviewcatalog);
        TextView textview = view.findViewById(R.id.textviewcatalog);
        Button button = view.findViewById(R.id.buttoncatalog);

        final Food food = _repository.getAllFood().get(position);

        image.setImageBitmap(BitmapFactory.decodeByteArray(food.getImage(), 0, food.getImage().length));
        textview.setText(food.getName());

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //MainActivity.callCalorieInformation(food);
                ((MainActivity) _context).callCalorieInformation(food);
            }
        };
        button.setOnClickListener(listener);

        return view;
    }
}
