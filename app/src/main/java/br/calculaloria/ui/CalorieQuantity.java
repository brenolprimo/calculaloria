package br.calculaloria.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import br.calculaloria.R;
import br.calculaloria.calculator.CalorieCalculator;
import br.calculaloria.food.Food;

public class CalorieQuantity extends AppCompatActivity {

    private Food _currentFood;

    private ImageView _imageView;
    private TextView _nameTextView;
    private EditText _quantityEditView;
    private Button _button;
    private TextView _hintTextView;
    private TextView _secondHintTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calorie_quantity);

        _imageView          = this.findViewById(R.id.imageview_information);
        _nameTextView       = this.findViewById(R.id.textview_information_name);
        _quantityEditView   = this.findViewById(R.id.edittext_information_quantity);
        _button             = this.findViewById(R.id.button_information_add);
        _hintTextView       = this.findViewById(R.id.textview_information_hint);
        _secondHintTextView = this.findViewById(R.id.textview_information_hint2);

        _currentFood = (Food) getIntent().getSerializableExtra("CurrentFood");

        this.initData();
    }

    private void initData() {
        if( _currentFood == null )
            this.onBackPressed();

        _imageView.setImageBitmap(
                BitmapFactory.decodeByteArray(
                        _currentFood.getImage(), 0, _currentFood.getImage().length
                )
        );
        _nameTextView.setText(_currentFood.getName());
        _quantityEditView.setHint("Quantidade em " + _currentFood.getMeasurement());
        String hint = String.format(
                Locale.US,
                "%d calorias em %d%s",
                _currentFood.getCalories(),
                _currentFood.getQuantity(),
                _currentFood.getMeasurement()
        );
        _hintTextView.setText(hint);
        _secondHintTextView.setText("Quantidade de calorias: 0 em 0" + _currentFood.getMeasurement());

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int count, int after) {
                int quantity = 0;

                try{
                    quantity = Integer.parseInt(charSequence.toString());
                }catch(NumberFormatException e) {
                    quantity = 0;
                }

                String message = String.format(
                        Locale.US,
                        "Quantidade de calorias: %d em %d%s",
                        Math.round(_currentFood.getBaseCalorie() * quantity),
                        quantity,
                        _currentFood.getMeasurement()
                );
                _secondHintTextView.setText(message);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        _quantityEditView.addTextChangedListener(watcher);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                DONT WORK
//                Intent tmp = new Intent();
//                tmp.putExtra("status","Everything finished ok");
//                setResult(Activity.RESULT_OK, tmp);
//                finish();

                int value = 0;
                try {
                    value = Integer.parseInt(_quantityEditView.getText().toString());
                } catch(NumberFormatException e) {
                    //finish();
                    Toast.makeText(getApplicationContext(), "Por favor informe um número válido!", Toast.LENGTH_SHORT ).show();
                    return;
                }

                if( value > 0 ) {
                    CalorieCalculator calculator = CalorieCalculator.getInstance();
                    calculator.addCalorie(value, _currentFood);
                }

                finish();
            }
        };
        _button.setOnClickListener(listener);
    }
}
