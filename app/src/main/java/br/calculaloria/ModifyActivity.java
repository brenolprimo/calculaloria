package br.calculaloria;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import br.calculaloria.database.SqliteDatabase;
import br.calculaloria.food.Food;
import br.calculaloria.food.repositories.FoodRepository;
import br.calculaloria.ui.adapter.CatalogAdapter;
import br.calculaloria.ui.adapter.ModifyAdapter;
import br.calculaloria.ui.gambiarra.Invalidable;

public class ModifyActivity extends AppCompatActivity implements Invalidable {

    private FoodRepository _repository;
    private FloatingActionButton _button;
    private ModifyAdapter _adapter;
    private GridView _catalog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify);

        this.init();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (resultCode) {
            case 10000:
                //Toast.makeText(this, "Dados foram atualizados!", Toast.LENGTH_SHORT).show();
                List<Invalidable> invalids = FoodRepository.getAllInvalidables();
                for(Invalidable invalid : invalids)
                    invalid.invalidade();

                break;
            case 10001:
                invalids = FoodRepository.getAllInvalidables();
                for(Invalidable invalid : invalids)
                    invalid.invalidade();
                break;
            default:
                Toast.makeText(this, "Dados não atualizados!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void invalidade() {
        _repository = new FoodRepository(new SqliteDatabase(this));
        _adapter = new ModifyAdapter(this, _repository);
        _catalog.invalidateViews();
        _catalog.setAdapter(_adapter);
    }

    public void startEditActivity(Food food) {
        Intent intent = new Intent(ModifyActivity.this, EditFoodActivity.class);
        intent.putExtra("foodname", food.getName());
        startActivityForResult(intent, 10001);
    }

    private void init() {
        _button = this.findViewById(R.id.button_add_food);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(ModifyActivity.this, "Clicked", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ModifyActivity.this, AddFoodActivity.class);
                //startActivity(intent);
                startActivityForResult(intent, 10000);
            }
        };
        _button.setOnClickListener(listener);

        _catalog = findViewById(R.id.gridview_modify_catalog);

        _repository = new FoodRepository(new SqliteDatabase(this));
        _adapter = new ModifyAdapter(this, _repository);

        _catalog.setAdapter(_adapter);

        FoodRepository.addInvalid(this);
    }
}
