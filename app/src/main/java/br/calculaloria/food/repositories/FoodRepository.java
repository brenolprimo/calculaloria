package br.calculaloria.food.repositories;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import br.calculaloria.database.Database;
import br.calculaloria.food.Food;
import br.calculaloria.ui.gambiarra.Invalidable;

public class FoodRepository implements PropertyChangeListener {

    private static List<Invalidable> _invalids = new ArrayList<>();

    private Database database;
    private List<Food> foodRepository;

    private static Food _savedFood;

    public FoodRepository( Database db ) throws NullPointerException {
        if( db == null )
            throw new NullPointerException("Database must be valid!");

        this.database = db;

        List<Food> foods = this.database.loadFoods();
        for(Food f : foods )
            f.addChangeListener(this);
        this.foodRepository = foods;
    }

    public List<Food> getAllFood() {
        return this.foodRepository == null ? new ArrayList<Food>() : this.foodRepository;
    }

    public void insertFood(Food food) {
        this.database.insertFood(food);
    }

    public void updateFood(Food food) {
        this.database.updateFood(food);
    }

    public static Food getSavedFood() {
        return _savedFood;
    }

    public static void saveFood(Food food) {
        _savedFood = food;
    }

    public static List<Invalidable> getAllInvalidables() { return _invalids; }

    public static void addInvalid(Invalidable invalid) {
        _invalids.add(invalid);
    }

    public static void removeInvalid(Invalidable invalid) {
        _invalids.remove(invalid);
    }

    public static void clearInvalids(Invalidable invalid) {
        _invalids.clear();
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if( !(propertyChangeEvent.getNewValue() instanceof Food) )
            return;

        Food foodSource = (Food) propertyChangeEvent.getNewValue();
        switch (propertyChangeEvent.getPropertyName()){
            case "delete":
                this.database.removeFood(foodSource);
                this.foodRepository.remove(foodSource);
                break;
            default:
                this.database.updateFood(foodSource);
        }
    }
}
