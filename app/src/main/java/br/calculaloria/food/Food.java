package br.calculaloria.food;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Locale;

public class Food implements Serializable
{
    private PropertyChangeSupport propertyChange = new PropertyChangeSupport(this);

    private String name, measurement;
    private int id, quantity, calories;
    private byte[] image;

    // Constructors

    /**
     * Creates a food object with every value mentioned in parameters.
     *
     * @param name - The name of the food.
     * @param measurement - The measurement type (eg. ml, g, unity, etc...)
     * @param quantity - The quantity based in the calories
     * @param calories - The calories based in the quantity
     */
    public Food (String name, byte[] image, String measurement, Integer quantity, Integer calories) {
        this.name        = name;
        this.image       = image;
        this.measurement = measurement.toLowerCase();
        this.quantity    = quantity;
        this.calories    = calories;
    }

    /**
    * Creates a food object with every value mentioned in parameters.
    *
    * @param id - The id of the food.
    * @param name - The name of the food.
    * @param measurement - The measurement type (eg. ml, g, unity, etc...)
    * @param quantity - The quantity based in the calories
    * @param calories - The calories based in the quantity
    */
    public Food ( int id, String name, byte[] image, String measurement, Integer quantity, Integer calories ) {
        this.id          = id;
        this.name        = name;
        this.image       = image;
        this.measurement = measurement.toLowerCase();
        this.quantity    = quantity;
        this.calories    = calories;
    }

    // Getters
    public int getId() { return this.id; }

    public String getName() { return this.name; }

    public String getMeasurement() { return this.measurement; }

    public int getQuantity() { return this.quantity; }

    public int getCalories() { return this.calories; }

    public double getBaseCalorie() { return ( (double) this.calories) / this.quantity; }

    public byte[] getImage() { return this.image; }

    // Setters
    public void setName( String name ) {
        if( name == null || name.equals("") )
            return;

        String oldName = this.name;
        this.name = name;
        this.propertyChange.firePropertyChange("name", oldName, this);
    }

    public void setMeasurement( String measurement ) {
        if( measurement == null || measurement.equals("") )
            return;

        String oldValue = this.measurement;
        this.measurement = measurement.toLowerCase();
        this.propertyChange.firePropertyChange("measurement", oldValue, this);
    }

    public void setQuantity( int quantity ) {
        if( quantity < 1 )
            return;

        int oldValue = this.quantity;
        this.quantity = quantity;
        this.propertyChange.firePropertyChange("quantity", oldValue, this);
    }

    public void setCalories( int calories ) {
        if( quantity < 1 )
            return;

        int oldValue = this.calories;
        this.calories = calories;
        this.propertyChange.firePropertyChange("calorie", oldValue, this);
    }

    public void setImage( InputStream stream ) {
        if( stream == null )
            return;

        byte[] oldValue = this.image;

        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();

        Bitmap bitmap = BitmapFactory.decodeStream(stream);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteStream);

        this.image = byteStream.toByteArray();

        this.propertyChange.firePropertyChange("image", oldValue, this);
    }

    public void setImage( byte[] array ) {
        byte[] oldValue = this.image;
        this.image = array;

        this.propertyChange.firePropertyChange("image", oldValue, this);
    }

    public void setFood( Food food ) {
        if( food == null )
            return;

        this.name = food.name;
        this.calories = food.calories;
        this.image = food.image;
        this.measurement = food.measurement;
        this.quantity = food.quantity;

        this.propertyChange.firePropertyChange("food", food.getId(), this);
    }

    public void addChangeListener(PropertyChangeListener listener) {
        this.propertyChange.addPropertyChangeListener(listener);
    }

    public void removeChangeListener (PropertyChangeListener listener) {
        this.propertyChange.removePropertyChangeListener(listener);
    }

    // Action
    public void deleteFood() {
        this.propertyChange.firePropertyChange("delete", this.id, this);
    }

    // Object overrides
    @Override
    public boolean equals(Object obj) {
        if( !(obj instanceof Food) )
            return false;
        
        Food food = (Food) obj;
        String thisFood = String.format(Locale.US,"%d%s%s%d%d",
            this.id,
            this.name,
            this.measurement,
            this.quantity,
            this.calories
        );
        String suspeciousFood = String.format(Locale.US,"%d%s%s%d%d",
            food.id,
            food.name,
            food.measurement,
            food.quantity,
            food.calories
        );

        return thisFood.equals(suspeciousFood);
    }

    @Override
    public String toString() {
        return this.name;
    }
}