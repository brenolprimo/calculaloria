package br.calculaloria.database;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import br.calculaloria.calculator.FoodHistory;
import br.calculaloria.food.Food;

/**
 * Base abstraction for persistance of all food handled
 * by the application in a database. Users are required to
 * handle the connection internally and only return the
 * {@link Food} result to the user. If the user want to
 * handle schema validation, the shouldCreateTable method
 * should be implemented.
*/
public interface Database
{
    public String getDatabaseName();

    /**
    * Base method for loading a single {@link Food} by name.
    * 
    * @param name - the name of the food to search
    * 
    * @return {@link Food} with the current name or empty if there isn't any.
    *
    * @throws {NullPointerException} if name is null or empty
    */
    public Food loadFood( String name );

    /**
     * Base method for loading a single {@link Food} by an id.
     *
     * @param id - the id of the food to search
     *
     * @return {@link Food} with the current name or empty if there isn't any.
     *
     * @throws {NullPointerException} if name is null or empty
     */
    public Food loadFood( int id );

    /**
    * Base method for inserting the {@link Food} into the
    * chosen persistance service.
    *
    * @return a non null array of {@link Food} retrieved from chosen pesistance service.
    */
    public List<Food> loadFoods();

    /**
    * Base method for inserting the {@link Food} into the
    * chosen persistance service.
    * 
    * @param food - non empty food structure with a unique name for insertion
    * 
    * @return long
    *
    * @throws {NullPointerException} if food is null or empty
    * @throws SQLIntegrityConstraintViolationException if the food's name already exists
    */
    public long insertFood( Food food );

    /**
    * Base method for updating the {@link Food} into the
    * chosen persistance service by using the name.
    * 
    * @param food - non empty food structure with a unique name to update
    * 
    * @return long.
    *
    * @throws {NullPointerException} if food is null or empty.
    * @throws SQLIntegrityConstraintViolationException if the food's name don't exist.
    */
    public long updateFood( String oldName, Food food );

    /**
     * Base method for updating the {@link Food} into the
     * chosen persistance service by using the id.
     *
     * @param food - non empty food structure with a unique name to update
     *
     * @return long.
     *
     * @throws {NullPointerException} if food is null or empty.
     * @throws SQLIntegrityConstraintViolationException if the food's name don't exist.
     */
    public long updateFood( Food food );

    /**
    * Base method for deleting the {@link Food} into the
    * chosen persistance service.
    * 
    * @param food - non empty food structure with a unique name to update
    * 
    * @return int - quantity of items deleted
    *
    * @throws {NullPointerException} if food is null or empty.
    * @throws SQLIntegrityConstraintViolationException if the food's name don't exist.
    */
    public int removeFood( Food food );

    long saveHistory(FoodHistory history);
    FoodHistory getFoodHistory(String historyName);
    long removeHistory(String historyName);
}