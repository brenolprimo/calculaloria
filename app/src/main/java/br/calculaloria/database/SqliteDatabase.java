package br.calculaloria.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import br.calculaloria.R;
import br.calculaloria.calculator.FoodHistory;
import br.calculaloria.food.Food;

// Image saving https://stackoverflow.com/questions/9357668/how-to-store-image-in-sqlite-database

public class SqliteDatabase implements Database
{
    private SQLiteDatabase database;

    public SqliteDatabase(Context context) {
        SqliteDatabaseConnection connection = new SqliteDatabaseConnection(context);
        this.database = connection.getWritableDatabase();
    }

    @Override
    public String getDatabaseName() {
        return SqliteDatabaseConnection.DATABASE_NAME;
    }

    @Override
    public Food loadFood(String name) {
        String[] columns = new String[]{
                "id",
                "name",
                "image",
                "measurement",
                "quantity",
                "calorie"
        };

        Cursor cursor = this.database.query(
                "Food",
                columns,
                "name = ?",
                new String[]{name},
                null,
                null,
                null
        );

        if( cursor == null )
            return null;

        Food food = null;
        while(cursor.moveToNext()) {
            food = new Food(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getBlob(2),
                    cursor.getString(3),
                    cursor.getInt(4),
                    cursor.getInt(5)
            );
        }
        cursor.close();

        return food;
    }

    public Food loadFood( int id ) {
        if( id < 1 )
            return null;

        String[] columns = new String[]{
                "id",
                "name",
                "image",
                "measurement",
                "quantity",
                "calorie"
        };

        Cursor cursor = this.database.query(
                "Food",
                columns,
                "id = ?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null
        );

        if( cursor == null )
            return null;

        Food food = null;
        while(cursor.moveToNext()) {
            food = new Food(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getBlob(2),
                    cursor.getString(3),
                    cursor.getInt(4),
                    cursor.getInt(5)
            );
        }
        cursor.close();

        return food;
    }

    @Override
    public List<Food> loadFoods() {
        String[] columns = new String[]{
                "id",
                "name",
                "image",
                "measurement",
                "quantity",
                "calorie"
        };
        List<Food> foods = new ArrayList<>();

        Cursor cursor = this.database.query(
                "food",
                columns,
                null,
                null,
                null,
                null,
                null
        );
        while(cursor.moveToNext()) {
            Food food = new Food(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getBlob(2),
                    cursor.getString(3),
                    cursor.getInt(4),
                    cursor.getInt(5)
            );
            foods.add(food);
        }

        cursor.close();
        return foods;
    }

    @Override
    public long insertFood(Food food) {
        ContentValues values = new ContentValues();
        values.put("name", food.getName());
        values.put("image", food.getImage());
        values.put("measurement", food.getMeasurement());
        values.put("quantity", food.getQuantity());
        values.put("calorie", food.getCalories());

        return this.database.insert("Food", null, values);
    }

    @Override
    public long updateFood(String oldName, Food food) {
        ContentValues values = new ContentValues();
        values.put("id", food.getId());
        values.put("name", food.getName());
        values.put("image", food.getImage());
        values.put("measurement", food.getMeasurement());
        values.put("quantity", food.getQuantity());
        values.put("calorie", food.getCalories());

        return this.database.update("food", values, "name = ?", new String[]{oldName});
    }

    @Override
    public long updateFood( Food food ) {
        if( food.getId() < 1 )
            return -1;

        ContentValues values = new ContentValues();
        values.put("id", food.getId());
        values.put("name", food.getName());
        values.put("image", food.getImage());
        values.put("measurement", food.getMeasurement());
        values.put("quantity", food.getQuantity());
        values.put("calorie", food.getCalories());

        return this.database.update(
                "food",
                values,
                "id = ?",
                new String[]{ String.valueOf(food.getId()) }
        );
    }

    @Override
    public int removeFood( Food food ) {
        return this.database.delete(
                "food",
                "id = ?",
                new String[] { String.valueOf(food.getId()) }
        );
    }

    @Override
    public long saveHistory(FoodHistory history) {
        if( history == null )
            return -1;

        String name = history.getName(true);
        byte[] historyClass = null;

        ByteArrayOutputStream oStream = new ByteArrayOutputStream();
        ObjectOutput serializer = null;

        try
        {
            try {
                serializer = new ObjectOutputStream(oStream);
                serializer.writeObject(history);
                serializer.flush();
                historyClass = oStream.toByteArray();
            } catch (IOException e) {
                int stop = 1;
            }
        }
        finally
        {
            try {
                oStream.close();
            } catch (IOException e) {

            }
        }

        if( historyClass == null )
            return -1;

        ContentValues values = new ContentValues();
        values.put("name", name);
        values.put("obj", historyClass);

        return this.database.insert("HistoryList", null, values );
    }

    public List<String> getAllFoodHistories() {
        String[] columns = new String[]{
                "name"
        };
        List<String> histories = new ArrayList<>();

        Cursor cursor = this.database.query(
                "HistoryList",
                columns,
                null,
                null,
                null,
                null,
                null
        );
        while(cursor.moveToNext()) {
            String name = cursor.getString(0);
            histories.add(name);
        }

        cursor.close();
        return histories;
    }

    public FoodHistory getFoodHistory(String historyName) {
        String[] columns = new String[] {
                "id",
                "name",
                "obj"
        };
        String[] selection = new String[] {
                historyName
        };
        FoodHistory history = null;
        Cursor cursor = this.database.query(
                "HistoryList",
                columns,
                "name = ?",
                selection,
                null,
                null,
                null
        );

        while(cursor.moveToNext()){
            byte[] serializedData = cursor.getBlob(2);
            ByteArrayInputStream iStream = new ByteArrayInputStream(serializedData);
            ObjectInput input = null;

            try {
                input = new ObjectInputStream(iStream);
                history = (FoodHistory) input.readObject();

                if(history != null)
                    history.setId(cursor.getInt(0));
            } catch (IOException|ClassNotFoundException e) {
                int stop = 1;
            }
        }

        return history;
    }

    @Override
    public long removeHistory(String historyName) {
        return this.database.delete(
                "HistoryList",
                "name = ?",
                new String[] { historyName }
        );
    }
}

class SqliteDatabaseConnection extends SQLiteOpenHelper
{
    static final String DATABASE_NAME= "banco.db";
    private static final int VERSION = 1;

    SqliteDatabaseConnection(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
        this.createDefaultData(context);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        StringBuilder sql = new StringBuilder();
        sql.append("CREATE TABLE 'Food' (")
                .append("'id' INTEGER PRIMARY KEY AUTOINCREMENT,")
                .append("'name' TEXT NOT NULL UNIQUE,")
                .append("'image' BLOB,")
                .append("'measurement' TEXT NOT NULL,")
                .append("'quantity' INTEGER NOT NULL,")
                .append("'calorie' INTEGER NOT NULL")
                .append(")");

        db.execSQL(sql.toString());
        sql.setLength(0);

        sql.append("CREATE TABLE 'HistoryList' (")
                .append("'id' INTEGER PRIMARY KEY AUTOINCREMENT,")
                .append("'name' TEXT NOT NULL UNIQUE,")
                .append("'obj' BLOB NOT NULL")
                .append(")");
        db.execSQL(sql.toString());
//        sql.setLength(0);
//
//        sql.append("CREATE TABLE 'HistoryValues' (")
//                .append("'history_id' INTEGER,")
//                .append("'food_id' INTEGER,")
//                .append("'food_calorie' INTEGER")
//                .append(")");
//        db.execSQL(sql.toString());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String sql = "DROP TABLE IF EXISTS 'FOOD'";
        db.execSQL(sql);
        onCreate(db);
    }

    private void createDefaultData(Context context) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        byte[] aguacoco = this.scaleAndLoadImage(
                BitmapFactory.decodeResource(
                        context.getResources(), R.drawable.agua_de_coco
                )
        );
        byte[] cafeacucar = this.scaleAndLoadImage(
                BitmapFactory.decodeResource(
                        context.getResources(), R.drawable.cafe_com_acucar
                )
        );
        byte[] cafesemacucar = this.scaleAndLoadImage(
                BitmapFactory.decodeResource(
                        context.getResources(), R.drawable.cafe_sem_acucar
                )
        );
        byte[] caldocana = this.scaleAndLoadImage(
                BitmapFactory.decodeResource(
                        context.getResources(), R.drawable.caldo_de_cana
                )
        );
        byte[] sucoabacaxi = this.scaleAndLoadImage(
                BitmapFactory.decodeResource(
                        context.getResources(), R.drawable.suco_de_abacaxi
                )
        );
        byte[] sucoacerola = this.scaleAndLoadImage(
                BitmapFactory.decodeResource(
                        context.getResources(), R.drawable.suco_de_acerola
                )
        );
        byte[] sucomaca = this.scaleAndLoadImage(
                BitmapFactory.decodeResource(
                        context.getResources(), R.drawable.suco_de_maca
                )
        );

        List<ContentValues> contentValues = new ArrayList<>(7);

        ContentValues values1 = new ContentValues();
        values1.put("name","Água de coco verde");
        values1.put("image", aguacoco);
        values1.put("measurement", "ml");
        values1.put("quantity", 240);
        values1.put("calorie", 62);

        ContentValues values2 = new ContentValues();
        values2.put("name","Café com açúcar");
        values2.put("image", cafeacucar);
        values2.put("measurement", "ml");
        values2.put("quantity", 50);
        values2.put("calorie", 33);

        ContentValues values3 = new ContentValues();
        values3.put("name","Café sem açúcar");
        values3.put("image", cafesemacucar);
        values3.put("measurement", "ml");
        values3.put("quantity", 40);
        values3.put("calorie", 3);

        ContentValues values4 = new ContentValues();
        values4.put("name","Caldo de cana");
        values4.put("image", caldocana);
        values4.put("measurement", "ml");
        values4.put("quantity", 240);
        values4.put("calorie", 202);

        ContentValues values5 = new ContentValues();
        values5.put("name","Suco de abacaxi natural");
        values5.put("image", sucoabacaxi);
        values5.put("measurement", "ml");
        values5.put("quantity", 240);
        values5.put("calorie", 100);

        ContentValues values6 = new ContentValues();
        values6.put("name","Suco de acerola natural");
        values6.put("image", sucoacerola);
        values6.put("measurement", "ml");
        values6.put("quantity", 240);
        values6.put("calorie", 36);

        ContentValues values7 = new ContentValues();
        values7.put("name","Suco de maçã natural");
        values7.put("image", sucomaca);
        values7.put("measurement", "ml");
        values7.put("quantity", 240);
        values7.put("calorie", 154);

        contentValues.add(values1);
        contentValues.add(values2);
        contentValues.add(values3);
        contentValues.add(values4);
        contentValues.add(values5);
        contentValues.add(values6);
        contentValues.add(values7);

        for(ContentValues value : contentValues) {
            db.insert("Food", null, value);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    private byte[] scaleAndLoadImage(Bitmap bitmap) {
        Bitmap b = Bitmap.createScaledBitmap(bitmap, 100, 150, true);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.JPEG, 80, baos);
        return baos.toByteArray();
    }
}
