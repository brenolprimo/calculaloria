package br.calculaloria;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;

import br.calculaloria.database.SqliteDatabase;
import br.calculaloria.food.Food;
import br.calculaloria.food.repositories.FoodRepository;

public class AddFoodActivity extends AppCompatActivity {

    private ImageView _image;
    private Button _buttonImage, _buttonAdd;
    private EditText _editTextName, _editTextMeasurement, _editTextCalorie, _editTextQuantity;

    private static final int IMAGE_PICK_CODE = 1000;
    private static final int PERMISSION_CODE = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_food);

        this.init();
    }

    private void init() {
        _image                  = findViewById(R.id.imageview_add_image);
        _buttonImage            = findViewById(R.id.button_add_image_choose);
        _buttonAdd              = findViewById(R.id.button_add_add);
        _editTextName           = findViewById(R.id.edittext_add_name);
        _editTextMeasurement    = findViewById(R.id.edittext_add_measurement);
        _editTextCalorie        = findViewById(R.id.edittext_add_calories);
        _editTextQuantity       = findViewById(R.id.edittext_add_quantity);

        View.OnClickListener editImagemListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ){
                    if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_DENIED )
                    {
                        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                        requestPermissions(permissions, PERMISSION_CODE);
                    }
                    else
                    {
                        getImageFromGallery();
                    }
                }
                else
                {
                    getImageFromGallery();
                }
            }
        };
        _buttonImage.setOnClickListener(editImagemListener);

        View.OnClickListener addListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                byte[] array = null;

                try {
                    Bitmap bitmap = ((BitmapDrawable) _image.getDrawable()).getBitmap();
                    ByteArrayOutputStream oStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, oStream);
                    array = oStream.toByteArray();
                    oStream.close();
                } catch(Exception e){
                    Log.d("CRASHOU", e.getMessage());
                }

                String name = _editTextName.getText().toString();
                String measurement = _editTextMeasurement.getText().toString();
                int calorie = Integer.parseInt(_editTextCalorie.getText().toString());
                int quantity = Integer.parseInt(_editTextQuantity.getText().toString());

                Food food = new Food(name, array, measurement, quantity, calorie);
                FoodRepository repository = new FoodRepository(new SqliteDatabase(AddFoodActivity.this));
                repository.insertFood(food);

                Toast.makeText(AddFoodActivity.this, "Adicionado com sucesso!", Toast.LENGTH_SHORT).show();

                Intent i = new Intent();
                i.putExtra("result", "ok");
                setResult(10000, i);
                finish();
            }
        };
        _buttonAdd.setOnClickListener(addListener);
    }

    private void getImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode)
        {
            case PERMISSION_CODE: {
                if( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED )
                {
                    getImageFromGallery();
                }
                else
                {
                    Toast.makeText(this, "Permissão negada...", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if( resultCode == RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            _image.setImageURI(data.getData());
        }
    }
}
